import paho.mqtt.client as mqtt

broker_url = "broker.emqx.io"
broker_port = 1883

def on_connect(client, userdata, flags, rc):
    print("Connected with Result Code {rc}")

def on_message(client, userdata, message):
    print("Message Received: "+message.payload.decode())



client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(broker_url, broker_port)

client.subscribe("sensor/#", qos=0)

client.publish(topic="TestingTopic", payload="TestingPayload", qos=1, retain=False)
client.loop_forever()
