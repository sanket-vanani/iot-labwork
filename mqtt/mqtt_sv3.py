import paho.mqtt.client as mqtt

def on_message(client, userdata, msg):
    print(msg.topic +" "+ str(msg.payload))

broker_url = "broker.emqx.io"

broker_port = 1883

client = mqtt.Client()
client.on_message = on_message

client.connect(broker_url, broker_port)

client.subscribe("TestingTopic1",qos=0)

client.loop_forever()
